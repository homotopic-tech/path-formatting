{-# LANGUAGE OverloadedStrings #-}

-- |
--   Module    : Path.Formatting
--   License   : MIT
--   Stability : experimental
--
-- Formatting for `Path`.
module Path.Formatting
  ( pathf,
  )
where

import Formatting
import Path

-- | Formats any `Path b t`.
--
-- @since 0.1.0.0
pathf :: Format r (Path b t -> r)
pathf = mapf toFilePath string
